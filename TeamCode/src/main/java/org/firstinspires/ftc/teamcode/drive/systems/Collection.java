package org.firstinspires.ftc.teamcode.drive.systems;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.util.ElapsedTime;

public class Collection {

    ElapsedTime time;
    private DcMotor cMotor1;
    private DcMotor cMotor2;

    public Collection (HardwareMap hwMap){
        cMotor1 = hwMap.dcMotor.get("cMotor1");
        cMotor2 = hwMap.dcMotor.get("cMotor2");
        time = new ElapsedTime();
        cMotor1.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        cMotor2.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        collectionOff();
    }

    public void collectionOff(){
        cMotor1.setPower(0);
        cMotor2.setPower(0);
    }

    public void collectionForward(double speed){
        cMotor1.setPower(1);
        cMotor2.setPower(1);
    }

    public void collectionBackward(double speed){
        cMotor1.setPower(-1);
        cMotor2.setPower(-1);
    }

    public void update(boolean collectionFwd, boolean collectionBwd){
        if(collectionFwd && !collectionBwd){
            collectionForward(1.0);
        }else if(!collectionFwd && collectionBwd){
            collectionBackward(1.0);
        }else if(!collectionFwd && !collectionBwd){
            collectionOff();
        }else{
            collectionOff();
        }
    }

}
