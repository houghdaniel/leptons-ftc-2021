package org.firstinspires.ftc.teamcode.drive.systems;

import com.acmerobotics.roadrunner.control.PIDFController;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.VoltageSensor;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

import org.openftc.revextensions2.ExpansionHubEx;

public class Shooter {

    ElapsedTime time;
    private DcMotor sLeft;
    private DcMotor sRight;
    private Servo sServo;
    private ExpansionHubEx hub;

    public Shooter(HardwareMap hwMap){
        sLeft = hwMap.dcMotor.get("sLeft");
        sRight = hwMap.dcMotor.get("sRight");
        sServo = hwMap.servo.get("sServo");
        hub = hwMap.get(ExpansionHubEx.class, "Expansion Hub 1");
        time = new ElapsedTime();
        sLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        sRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        setShooterSpeed(0.0);
    }

    public void setShooterSpeed(double speed){
        double power = speed * 12.0 / (hub.read12vMonitor(ExpansionHubEx.VoltageUnits.VOLTS));

        sLeft.setPower(power);
        sRight.setPower(power);
    }

    public void shoot(double shoot){
        double goPos = 0.5;
        double noPos = 0.0;

        if(shoot > 0){
            sServo.setPosition(Range.clip(goPos, Servo.MIN_POSITION, Servo.MAX_POSITION));
        }else{
            sServo.setPosition(Range.clip(noPos, Servo.MIN_POSITION, Servo.MAX_POSITION));
        }

    }

}
