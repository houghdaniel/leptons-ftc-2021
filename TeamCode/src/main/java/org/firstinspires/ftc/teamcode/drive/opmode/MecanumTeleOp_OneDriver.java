package org.firstinspires.ftc.teamcode.drive.opmode;


import com.acmerobotics.roadrunner.geometry.Pose2d;
import org.firstinspires.ftc.teamcode.drive.systems.SampleMecanumDrive;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.HardwareMap;

import com.acmerobotics.roadrunner.drive.MecanumDrive;
import org.firstinspires.ftc.teamcode.drive.systems.Collection;
import org.firstinspires.ftc.teamcode.drive.systems.Shooter;

@TeleOp(name="MecanumTeleOp_OneDriver")
public class MecanumTeleOp_OneDriver extends LinearOpMode {

    private boolean collectionBwd = false;
    private boolean collectionFwd = false;


    @Override
    public void runOpMode(){

        MecanumDrive drive = new SampleMecanumDrive(hardwareMap);
        Collection collectionSys = new Collection(hardwareMap);
        Shooter shooter = new Shooter(hardwareMap);

        waitForStart();

        while(opModeIsActive()){

            //DRIVE
            drive.setDrivePower(new Pose2d(-gamepad1.left_stick_x,gamepad1.left_stick_y,gamepad1.right_stick_x));

            //COLLECTION
            if(gamepad1.a){
                collectionFwd = true;
                collectionBwd = false;
            }else if(gamepad1.b){
                collectionBwd = true;
                collectionFwd = false;
            }else if(gamepad1.x){
                collectionFwd = false;
                collectionBwd = false;
            }

            collectionSys.update(collectionFwd, collectionBwd);

            //SHOOTER
            if(gamepad1.left_trigger > 0){
                shooter.setShooterSpeed(0.8);
            }else{
                shooter.setShooterSpeed(0.0);
            }

            shooter.shoot(gamepad1.right_trigger);



        }

    }

}
