package org.firstinspires.ftc.teamcode.drive.opmode;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.drive.systems.Shooter;
import org.openftc.revextensions2.ExpansionHubEx;

import org.firstinspires.ftc.teamcode.drive.systems.Collection;
import org.firstinspires.ftc.teamcode.drive.systems.SampleMecanumDrive;

@TeleOp(name="MecanumTeleOp_TwoDrivers")
public class MecanumTeleOp_TwoDrivers extends LinearOpMode {

    private boolean collectionBwd = false;
    private boolean collectionFwd = false;

    @Override
    public void runOpMode(){

        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);
        Collection collectionSys = new Collection(hardwareMap);
        Shooter shooter = new Shooter(hardwareMap);

        waitForStart();

        while(opModeIsActive()){

            drive.setDrivePower(new Pose2d(gamepad1.left_stick_y,gamepad1.left_stick_x,gamepad1.right_stick_x));

            if(gamepad2.a){
                collectionFwd = true;
                collectionBwd = false;
            }else if(gamepad2.b){
                collectionBwd = true;
                collectionFwd = false;
            }else if(gamepad2.x){
                collectionFwd = false;
                collectionBwd = false;
            }

            collectionSys.update(collectionFwd, collectionBwd);

            if(gamepad2.left_trigger > 0){
                shooter.setShooterSpeed(0.8);
            }else{
                shooter.setShooterSpeed(0.0);
            }

            shooter.shoot(gamepad2.right_trigger);


        }

    }

}
