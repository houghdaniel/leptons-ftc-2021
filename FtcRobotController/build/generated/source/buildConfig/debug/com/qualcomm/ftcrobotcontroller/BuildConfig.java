/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.qualcomm.ftcrobotcontroller;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.qualcomm.ftcrobotcontroller";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 39;
  public static final String VERSION_NAME = "6.1";
  // Fields from default config.
  public static final String BUILD_TIME = "2021-02-20T22:16:27.784-0600";
}
